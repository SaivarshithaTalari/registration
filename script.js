function validateForm() {
      const username = document.getElementById("username").value;
      const email = document.getElementById("email").value;
      const mobile = document.getElementById("mobile").value;
      const password = document.getElementById("password").value;
      const confirmPassword = document.getElementById("confirmPassword").value;

      // Complex username validation - allow only alphanumeric characters and underscores
      const usernameRegex = /^[a-zA-Z0-9_]+$/;
      if (!usernameRegex.test(username)) {
            alert("Username can only contain letters, numbers, and underscores!");
            return;
      }

      // Complex password validation - enforce at least 8 characters including uppercase, lowercase, and a number
      const passwordRegex = /^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)[a-zA-Z\d@$!%*?&]{8,}$/;
      if (!passwordRegex.test(password)) {
            alert("Password must contain at least 8 characters with at least one uppercase letter, one lowercase letter, and one number!");
            return;
      }

      // Check if the passwords match
      if (password !== confirmPassword) {
            alert("Passwords do not match!");
            return;
      }

      // Check for email format using regular expression
      const emailRegex = /^[^\s@]+@[^\s@]+\.[^\s@]+$/;
      if (!emailRegex.test(email)) {
            alert("Invalid email address!");
            return;
      }

      // Check if the mobile number is of 10 digits
      if (mobile.length !== 10) {
            alert("Mobile number should be 10 digits!");
            return;
      }

      // If all validations pass, show success message
      alert("Registration successful!");
}
